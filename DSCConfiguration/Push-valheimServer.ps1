﻿Configuration Push-valheimServer
{
param(
    [Parameter(Mandatory=$true)][String]$Name_Server,
    [Parameter(Mandatory=$true)][String]$Name_world,
    [Parameter(Mandatory=$true)][String]$Password,
    [Parameter(Mandatory=$false)][String]$path_of_save = 'c:\dsgames\dsg-server\valheim\save'
    )


Import-DscResource -ModuleName 'PSDesiredStateConfiguration', 'FileDownloadDSC', 'cFirewall', 'FileContentDsc'

    Node localhost
    {
        File DirectoryValheim {
            Type            = 'Directory'
            DestinationPath = 'c:\dsgames\dsg-server\valheim\'
            Ensure          = 'Present'
        }
        Script InstallValheim{           
            SetScript = {
                Start-Process -FilePath "c:\dsgames\dsg-tools\steamcmd\Steamcmd.exe" -ArgumentList "+login anonymous +force_install_dir ""c:\dsgames\dsg-server\valheim"" +app_update 896660 validate +exit" -Wait -PassThru -NoNewWindow;
            }
            TestScript = { 
                Test-Path 'c:\dsgames\dsg-server\valheim\valheim_server.exe'
            }
            GetScript = { 
                @{ Result = (Test-Path 'c:\dsgames\dsg-server\valheim\valheim_server.exe') } 
            }
        }
        cFirewallRule Valheim-TCP {
            Name        = 'Valheim-TCP'
            Description = 'Valheim-server (TCP-in)'
            Action      = 'Allow'
            Direction   = 'Inbound'
            LocalPort   = ('2456-2458', '27015-27030', '27036-27037')
            Protocol    = 'TCP'
            Profile     = 'All'
            Enabled     = $True
            Ensure      = 'Present'
        }
        cFirewallRule Valheim-UDP {
            Name        = 'Valheim-UDP'
            Description = 'Valheim-server (UDP-in)'
            Action      = 'Allow'
            Direction   = 'Inbound'
            LocalPort   = ('2456-2458', '27000-27031', '4380', '27036')
            Protocol    = 'UDP'
            Profile     = 'All'
            Enabled     = $True
            Ensure      = 'Present'
        }
        ReplaceText NameServer {
           Path   = 'c:\dsgames\dsg-server\valheim\start_headless_server.bat'
           Search = 'My server'
           Type   = 'Text'
           Text   = "$Name_Server"
        }
        ReplaceText NameWorld {
           Path   = 'c:\dsgames\dsg-server\valheim\start_headless_server.bat'
           Search = 'Dedicated'
           Type   = 'Text'
           Text   = "$Name_world"
        }
        ReplaceText Password {
            Path   = 'c:\dsgames\dsg-server\valheim\start_headless_server.bat'
            Search = 'secret'
            Type   = 'Text'
            Text   = "$Password"" -savedir ""$path_of_save"" -public 1"
        }
    }
}

Push-valheimServer -OutputPath $PSScriptRoot `
                   -Name_Server "test" `
                   -Name_world "test" `
                   -Password "test"