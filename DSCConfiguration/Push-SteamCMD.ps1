Configuration Push-SteamCMD
{
param()


Import-DscResource -ModuleName 'PSDesiredStateConfiguration', 'FileDownloadDSC', '7ZipArchiveDsc'

    Node localhost
    {
         File SteamCMD {
            Type            = 'Directory'
            DestinationPath = 'C:\tmp'
            Ensure          = "Present"
         }

         FileDownload SteamCMD {
            FileName  = "c:\tmp\steamcmd.zip"
            Url       = "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip"
            DependsOn = "[File]SteamCMD"
        }
        
        x7ZipArchive livraison {
			Path        = "c:\tmp\steamcmd.zip"
			Destination = "c:\dsgames\dsg-tools\steamcmd\"
			Clean       = $true
            DependsOn   = "[FileDownload]SteamCMD"
		}
        
        WindowsProcess SteamCMD {
            path             = "c:\dsgames\dsg-tools\steamcmd\steamcmd.exe"
            Arguments        = "+login anonymous +exit"
            WorkingDirectory = "c:\dsgames\dsg-tools\steamcmd\"
            Ensure           = "present"
            DependsOn        = "[x7ZipArchive]livraison"
        }
    }
}

Push-SteamCMD -OutputPath $PSScriptRoot