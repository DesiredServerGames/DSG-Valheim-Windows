## Ansible playbook: Valheim

[README English version](./README.EN.md)

## Description
Installation, configure et sécurisation d'un serveur Valheim sur windows (WS 10-11/WS server 2012-2016-2019).

## Visuels
__WINDOWS 10__

_Youtube Bientôt disponible_

__WINDOWS SERVER 2016__

_Youtube Bientôt disponible_

## Préalable
Vérifier au préalable l'état d'autorisation d'éxecution de scripts. 

Toutes les commandes doivent être lancer en mode ADMINISTRATEUR:

```powershell
PS C:\WINDOWS\system32> Get-ExecutionPolicy
RemoteSigned

PS C:\WINDOWS\system32> Set-ExecutionPolicy Unrestricted
"Modification de la stratégie d'exécution La stratégie d’exécution permet de vous prémunir contre les scripts que vous jugez non fiables. En modifiant la stratégie d’exécution, vous vous exposez aux risques de sécurité décrits dans la rubrique d’aide about_Execution_Policies à l’adresse https://go.microsoft.com/fwlink/?LinkID=135170. Voulez-vous modifier la stratégie d’exécution ?
[O] Oui  [T] Oui pour tout  [N] Non  [U] Non pour tout  [S] Suspendre  [?] Aide (la valeur par défaut est « N ») :" T

PS C:\WINDOWS\system32> Get-ExecutionPolicy
Unrestricted
```

## Role Variables
Changer les valeurs selon votre convenance, les paramètres avec le mandatory à `$False` ne sont pas obligatoires car définit par une valeur par défaut.

Si vous voulez les modifiées, rajouter le paramètre à la dernière ligne de commande en surchargeant la valeur.

**DSCConfiguration\Push-valheimServer.PS1**
```powershell
Configuration Push-valheimServer
{
param(
    [Parameter(Mandatory=$true)][String]$Name_Server,
    [Parameter(Mandatory=$true)][String]$Name_world,
    [Parameter(Mandatory=$true)][String]$Password,
    [Parameter(Mandatory=$false)][String]$path_of_save = 'c:\dsgames\dsg-server\valheim\save'
    )
...
...
...
}
Push-valheimServer -OutputPath $PSScriptRoot `
                   -Name_Server "test" `
                   -Name_world "test" `
                   -Password "test"
```

## Dependances
Pas de dépendances


## Utilisation
EN MODE ADMINISTRATEUR
```powershell
PS C:\WINDOWS\system32> cd C:\DSG-Valheim-Windows\
PS C:\DSG-Valheim-Windows> .\Deploy-ValheimServer.PS1
VERBOSE: Preparation a l'installation des modules DSC
COMMENTAIRES : Acquiring providers for assembly:
....
```
## Support
Utiliser les issues prévu à cette effet ou le [discord](https://discord.gg/pt6DXQxvZs 'DISCORD - Desired Games Server') pour de plus amples informations

## Roadmap
Pour l'instant rien

## License
MIT

## Author Information
Ce rôle a été créé en 2021 par Didier MINOTTE.

